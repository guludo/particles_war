This is Particles War.

Particles War simulates a battle between two teams of particles.

This program depends on the following libraries:
    - libncurses
    - libann:
        - Source files for this lib can be found at https://gustavo_sousa@bitbucket.org/gustavo_sousa/computacao_natureza.git
        - Arch Linux packages can be built from https://gustavo_sousa@bitbucket.org/gustavo_sousa/arch-packages.git
