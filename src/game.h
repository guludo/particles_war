#ifndef __game_h
#define __game_h

struct game;

#include "map.h"
#include "particle.h"

#define GAME_PARAM_BULLET_RANGE 20
#define GAME_PARAM_BULLET_DAMAGE .1
#define GAME_PARAM_MAX_TURNS 600

#define GAME_MAP_PARTICLE_ROOM 3

#define game_each_particle(game, p) \
{ \
    struct particle *p; \
    struct team *__t[2] = {&(game)->team_a, &(game)->team_b}; \
    int __i; \
    for (__i = 0; __i < 2; __i++) { \
        int __j; \
        for (__j = 0; __j < __t[__i]->num_particles; __j++) { \
            p = __t[__i]->particles + __j;

#define game_each_particle_end \
        } \
    } \
}

struct game;

struct team {
    int num_particles;
    struct particle *particles;
};

struct game_params {
    /* number of cells a bullet can reach */
    int bullet_range;
    /* The health damage a bullet causes. Values from 0 to 1. */
    double bullet_damage;
    int max_turns;
};

enum game_event_type {
    EVENT_PARTICLE_SHOT,
    EVENT_PARTICLE_KILLED
};

struct game_event {
    enum game_event_type type;
    struct particle *particle;
    struct particle *origin_particle;
};

struct game_events {
    struct game_event *events;
    int num_events;
};

struct game_callbacks {
    int (*start)(struct game *g);
    void (*turn)(struct game *g);
    void (*finish)(struct game *g);
};

struct team_controller {
    const char *name;
    struct game *game;
    struct team *team;
    struct team *opponent;
    /*
     * Boolean indicating if the team is training.
     */
    int training;
    /*
     * Controllers can use this pointer for specific purposes.
     */
    void *data;
    /*
     * Function that is called for each team on each loop.
     * The parameter actions must be filled with an action for each particle.
     */
    void (*actions)(struct team_controller *controller,
                    struct particle_action *actions);
    /*
     * Function called when a match has been started.
     */
    void (*match_started)(struct team_controller *controller);
    /*
     * Function called when a match has finished.
     */
    void (*match_finished)(struct team_controller *controller);
};

struct game {
    struct game_params params;
    struct game_callbacks callbacks;
    struct team team_a;
    struct team_controller team_a_controller;
    struct particle_action *actions_a;
    int dead_count_a;
    struct team team_b;
    struct team_controller team_b_controller;
    struct particle_action *actions_b;
    int dead_count_b;
    struct map map;
    /* stores the particles that are alive */
    struct particle **particle_map;
    int turn;
    struct game_events turn_events;
};

int game_init(struct game *game, int num_particles_a, int num_particles_b);
int game_run(struct game *game);
int game_destroy(struct game *game);

struct particle *shot_hits_particle(struct game *g, struct particle *shooter,
                                    struct vector *bullet_last_position);
#endif //__game_h
