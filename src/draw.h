#ifndef __draw_h
#define __draw_h

struct _draw;

#include "game.h"

typedef struct _draw {
    void *data;
    struct game *game;
} draw_t;

draw_t *draw_init(struct game *game);
int draw_destroy(draw_t *draw);
void draw_refresh(draw_t *draw);

#endif //__draw_h
