#include <errno.h>
#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>

#include "draw.h"

#define MAP_EMPTY_PAIR 1
#define MAP_BLOCK_PAIR 2
#define TEAM_A_PAIR 3
#define TEAM_B_PAIR 4

struct draw {
};

draw_t *draw_init(struct game *game)
{
    draw_t *draw = malloc(sizeof(draw_t));

    if (draw == NULL)
        return NULL;

    draw->game = game;
    draw->data = malloc(sizeof(struct draw));
    if (draw->data == NULL)
        goto error_cleanup;

    if (initscr() == NULL)
        goto error_cleanup;

    if (!has_colors()) {
        endwin();
        fprintf(stderr, "draw: terminal doesn't support colors\n");
        goto error_cleanup;
    }
    start_color();
    if (init_color(COLOR_BLACK, 0, 0, 0) == ERR) {
        fprintf(stderr, "init color failed\n");
    }
    use_default_colors();

    init_pair(MAP_EMPTY_PAIR, COLOR_BLACK, COLOR_BLACK);
    init_pair(MAP_BLOCK_PAIR, COLOR_WHITE, COLOR_WHITE);
    init_pair(TEAM_A_PAIR, COLOR_BLUE, COLOR_BLACK);
    init_pair(TEAM_B_PAIR, COLOR_RED, COLOR_BLACK);

    curs_set(0);

    return draw;

error_cleanup:
    if (draw != NULL) {
        if (draw->data != NULL) {
            free(draw->data);
        }
        free(draw);
    }
    return NULL;
}

int draw_destroy(draw_t *draw)
{
    free(draw->data);
    free(draw);
    endwin();
    return 0;
}

static void draw_map(struct map *m, int y0, int x0)
{
    int x, y;
    for (x = 0; x < m->x_len; x++) {
        for (y = 0; y < m->y_len; y++) {
            enum map_cell c = m->cells[x * m->y_len + y];
            short pair = c == BLOCK ? MAP_BLOCK_PAIR : MAP_EMPTY_PAIR;
            mvaddch(y0 + y, x0 + x, ' ' | COLOR_PAIR(pair));
        }
    }
}

static void draw_team(struct team *t, int y0, int x0, short pair)
{
    int i;
    for (i = 0; i < t->num_particles; i++) {
        struct particle *p = t->particles + i;

        if (p->health > 0)
            continue;

        mvaddch(y0 + p->pos.y, x0 + p->pos.x,
                'x' | COLOR_PAIR(pair));
    }
    for (i = 0; i < t->num_particles; i++) {
        struct particle *p = t->particles + i;

        if (p->health <= 0)
            continue;

        mvaddch(y0 + p->pos.y, x0 + p->pos.x,
                'o' | COLOR_PAIR(pair));
    }
}

static void draw_bullets(struct team *team,
                         struct particle_action *actions,
                         int y0, int x0,
                         int t,  /* time */
                         short pair)
{
    int i;
    for (i = 0; i < team->num_particles; i++) {
        if (actions[i].action != FIRE)
            continue;

        struct particle *p = team->particles + i;
        struct vector b = p->pos; /* bullet */
        b.x += t * p->heading.x;
        b.y += t * p->heading.y;

        if (abs(b.x - p->pos.x) <= abs(actions[i].v.x - p->pos.x) &&
            abs(b.y - p->pos.y) <= abs(actions[i].v.y - p->pos.y)) {
            mvaddch(y0 + b.y, x0 + b.x,
                    '+' | COLOR_PAIR(pair));
        }
    }
}

static void print_team_status(struct team *t, short pair)
{
    int i;
    double health = 0;
    int alive = 0;
    for (i = 0; i < t->num_particles; i++) {
        struct particle *p = t->particles + i;
        if (p->health > 0) {
            health += p->health;
            alive++;
        }
    }
    attron(COLOR_PAIR(pair));
    printw("Health: %.2f%%  Particles: %d\n",
           100 * health / t->num_particles,
           alive);
    attroff(COLOR_PAIR(pair));
}

void draw_refresh(draw_t *draw)
{
    struct game *g = draw->game;
    int i;

    /* animate bullets */
    for (i = 1; i < g->params.bullet_range; i++) {
        draw_bullets(&g->team_a, g->actions_a, 1, 1, i, TEAM_A_PAIR);
        draw_bullets(&g->team_b, g->actions_b, 1, 1, i, TEAM_B_PAIR);
        refresh();
        usleep(10000);
    }

    clear();
    draw_map(&g->map, 1, 1);
    draw_team(&g->team_a, 1, 1, TEAM_A_PAIR);
    draw_team(&g->team_b, 1, 1, TEAM_B_PAIR);
    move(2 + g->map.y_len, 0);
    printw("Turn %d/%d\n", g->turn, g->params.max_turns);

    printw("Team A (ctl=%s):\t", g->team_a_controller.name);
    print_team_status(&g->team_a, TEAM_A_PAIR);

    printw("Team B (ctl=%s):\t", g->team_b_controller.name);
    print_team_status(&g->team_b, TEAM_B_PAIR);

    refresh();
}
