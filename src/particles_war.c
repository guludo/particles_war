#define _GNU_SOURCE
#include <errno.h>
#include <getopt.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "draw.h"
#include "game.h"
#include "map.h"
#include "random_team_controller.h"
#include "greedy_team_controller.h"
#include "ann_team_controller.h"

#define DEFAULT_NUM_PARTICLES 10
#define DEFAULT_MAP_X_DIMENSION 80
#define DEFAULT_MAP_Y_DIMENSION 40

static struct {
    const char *name;
} team_controllers[] = {
    {"random"},
    {"ann"},
    {"greedy"},
    {NULL}
};

static int team_controller_index(const char *name)
{
    int i;
    for (i = 0; team_controllers[i].name != NULL; i++)
        if (strcmp(team_controllers[i].name, name) == 0)
            return i;
    return -1;
}

static const char *renderers[] = {
    "none",
    "ncurses",
    NULL
};

static int array_find_str(const char *arr[], const char *str)
{
    int i;
    for (i = 0; arr[i] != NULL; i++)
        if (strcmp(arr[i], str) == 0)
            return i;
    return -1;
}

enum controller_arg {
    CONTROLLER_RANDOM,
    CONTROLLER_ANN,
    CONTROLLER_GREEDY
};

static struct {
    struct {
        int num_particles;
        enum controller_arg controller;
    } team_a, team_b;
    /* map_dimension is used if an empty map is used */
    struct vector map_dimension;
    /* use of custom map */
    const char *map_filename;
    int num_training_matches;
    enum {
        RENDERING_NONE = 0,
        RENDERING_NCURSES
    } rendering;
} particles_war_args = {
    .team_a = {
        .num_particles = DEFAULT_NUM_PARTICLES,
        .controller = CONTROLLER_RANDOM
    },
    .team_b = {
        .num_particles = DEFAULT_NUM_PARTICLES,
        .controller = CONTROLLER_RANDOM
    },
    .map_dimension = {
        .x = DEFAULT_MAP_X_DIMENSION,
        .y = DEFAULT_MAP_Y_DIMENSION
    },
    .map_filename = NULL,
    .num_training_matches = 0,
    .rendering = RENDERING_NCURSES
};

draw_t *drawer;

int init_random()
{
    struct timespec tp;
    if (clock_gettime(CLOCK_MONOTONIC, &tp) < 0)
        return -1;
    srandom((unsigned int)tp.tv_nsec);
    return 1;
}

static struct option longopts[] = {
    {"map-x", required_argument, NULL, 'x'},
    {"map-y", required_argument, NULL, 'y'},
    {"a-particles", required_argument, NULL, 'a'},
    {"a-controller", required_argument, NULL, 'A'},
    {"b-particles", required_argument, NULL, 'b'},
    {"b-controller", required_argument, NULL, 'B'},
    {"training", required_argument, NULL, 't'},
    {"rendering", required_argument, NULL, 'r'},
    {"help", no_argument, NULL, 'h'},
    {}
};
static const char *optstr = "x:y:a:A:b:B:t:r:h";
static int longindex;

static void help(FILE *stream)
{
    fprintf(stream,
"Usage: %s [OPTIONS}\n"
"\n"
"Options:\n"
"\n"
"    -x --map-x=size\n"
"        The size of the map on the x-axis.\n"
"\n"
"    -y --map-y=size\n"
"        The size of the map on the y-axis.\n"
"\n"
"    -a --a-particles, -b --b-particles\n"
"        Number of particles for either team A or B.\n"
"\n"
"    -A --a-controller, -B --b-controller\n"
"        The controller for the teams. Currently only \"random\" is supported.\n"
"\n"
"    -t --train=num_matches\n"
"        Do num_matches training matches before.\n"
"\n"
"    -r --rendering=rendering\n"
"        Which type of rendering to use. Default is \"ncurses\". Currently\n"
"        only \"ncurses\" rendering is available. Use \"none\" for no\n"
"        rendering.\n"
"\n"
"    -h --help\n"
"        Display this help message and exit.\n"
, program_invocation_name);
}

static int parse_positive_option(int *dest)
{
    *dest = atoi(optarg);
    if (*dest <= 0) {
        fprintf(stderr, "%s: option %s must be a positive integer\n",
                program_invocation_name, longopts[longindex].name);
        errno = EINVAL;
        return -1;
    }
    errno = 0;
    return 0;
}

static int parse_args(int argc, char *argv[])
{
    int o;
    int r;
    int first_errno = 0;
    while ((o = getopt_long(argc, argv, optstr, longopts, &longindex)) != -1) {
        r = 0;
        switch (o) {
        case 'x':
            r = parse_positive_option(&particles_war_args.map_dimension.x);
            break;
        case 'y':
            r = parse_positive_option(&particles_war_args.map_dimension.y);
            break;
        case 'a':
            r = parse_positive_option(&particles_war_args.team_a.num_particles);
            break;
        case 'b':
            r = parse_positive_option(&particles_war_args.team_b.num_particles);
            break;
        case 'A':
        case 'B':
            /* verify if controller is valid */
            r = team_controller_index(optarg);
            if (r < 0) {
                int i;
                fprintf(stderr,
                        "%s: invalid team controller identifier \"%s\"\n"
                        "    List of valid identifiers:\n",
                        program_invocation_name,
                        optarg);
                for (i = 0; team_controllers[i].name != NULL; i++)
                    fprintf(stderr, "    %s\n", team_controllers[i].name);
                errno = EINVAL;
                break;
            }
            if (o == 'A')
                particles_war_args.team_a.controller = r;
            else
                particles_war_args.team_b.controller = r;
            break;
        case 't':
            particles_war_args.num_training_matches = atoi(optarg);
            if (particles_war_args.num_training_matches < 1) {
                fprintf(stderr, "%s: number of training matches must be at least 1\n",
                        program_invocation_name);
                errno = EINVAL;
                r = -1;
            }
            break;
        case 'r':
            r = array_find_str(renderers, optarg);
            if (r < 0) {
                int i;
                fprintf(stderr,
                        "%s: invalid renderering identifier \"%s\"\n"
                        "    List of valid identifiers:\n",
                        program_invocation_name,
                        optarg);
                for (i = 0; renderers[i] != NULL; i++)
                    fprintf(stderr, "    %s\n", renderers[i]);
                errno = EINVAL;
                break;
            }
            particles_war_args.rendering = r;
            break;
        case 'h':
            help(stdout);
            exit(0);
            break;
        case '?':
            help(stderr);
            exit(1);
            break;
        }

        if (r < 0 && !first_errno)
            first_errno = errno;
    }

    errno = first_errno;
    return errno == 0 ? 0 : -1;
}

static int start_callback(struct game *g)
{
    if (particles_war_args.rendering != RENDERING_NONE)
        draw_refresh(drawer);
    return 0;
}

static void turn_callback(struct game *g)
{
    if (particles_war_args.rendering == RENDERING_NONE)
        return;
    draw_refresh(drawer);
    usleep(250000);
}

static void finish_callback(struct game *g)
{
}

static int init_team_controller(struct team_controller *ctl,
                                enum controller_arg ctl_arg)
{
    int r = 0;
    switch (ctl_arg) {
        case CONTROLLER_RANDOM:
            random_team_controller(ctl);
            errno = 0;
            break;
        case CONTROLLER_GREEDY:
            greedy_team_controller(ctl);
            errno = 0;
            break;
        case CONTROLLER_ANN:
        {
            struct annctl_params params;
            annctl_init_params(&params);
            r = annctl(ctl, &params);
            break;
        }
    }
    return r;
}

static int init_team_controllers(struct game *g)
{
    int r;

    r = init_team_controller(&g->team_a_controller,
                             particles_war_args.team_a.controller);
    if (r < 0)
        return r;

    r = init_team_controller(&g->team_b_controller,
                             particles_war_args.team_b.controller);
    return r;
}

static int destroy_team_controllers(struct game *g)
{
    if (particles_war_args.team_a.controller == CONTROLLER_ANN) {
        annctl_destroy(&g->team_a_controller);
    }
    if (particles_war_args.team_b.controller == CONTROLLER_ANN) {
        annctl_destroy(&g->team_b_controller);
    }
    return 0;
}

int main(int argc, char *argv[])
{
    int r;
    int i;
    struct game game;

    r = parse_args(argc, argv);
    if (r < 0) {
        fprintf(stderr, "%s: error on parsing args: %m\n",
                program_invocation_name);
        return r;
    }

    r = init_random();
    if (r < 0)
        return r;

    r = game_init(&game,
            particles_war_args.team_a.num_particles,
            particles_war_args.team_b.num_particles);
    if (r < 0) {
        fprintf(stderr, "particles_war: error on initializing: %m\n");
        return r;
    }

    r = map_init_empty(&game.map,
                       particles_war_args.map_dimension.x,
                       particles_war_args.map_dimension.y);
    if (r < 0) {
        fprintf(stderr, "particles_war: error on creating map: %m\n");
        goto cleanup;
    }

    r = init_team_controllers(&game);
    if (r < 0) {
        int e = errno;
        fprintf(stderr,
                "particles_war: error on initializing team controllers: %m\n");
        errno = e;
        goto cleanup;
    }

    /* do training sessions beforehand */
    game.team_a_controller.training = 1;
    game.team_b_controller.training = 1;
    for (i = 0; i < particles_war_args.num_training_matches; i++) {
        printf("\rTraining session %d/%d",
                i + 1, particles_war_args.num_training_matches);
        r = game_run(&game);
        if (r < 0) {
            fprintf(stderr, "particles_war: error on running game: %m\n");
            break;
        }
    }
    if (particles_war_args.num_training_matches > 0)
        printf("\n");

    game.callbacks.start = &start_callback;
    game.callbacks.turn = &turn_callback;
    game.callbacks.finish = &finish_callback;

    if (particles_war_args.rendering != RENDERING_NONE) {
        drawer = draw_init(&game);
        if (drawer == NULL) {
            fprintf(stderr,
                    "particles_war: error on initializing renderer: %m\n");
            goto cleanup;
        }
    }

    /* now itś time for the real thing! */
    game.team_a_controller.training = 0;
    game.team_b_controller.training = 0;
    r = game_run(&game);
    if (r < 0) {
        fprintf(stderr, "particles_war: error on running game: %m\n");
        goto cleanup;
    }

    if (particles_war_args.rendering != RENDERING_NONE)
        getch();

cleanup:
    destroy_team_controllers(&game);
    if (particles_war_args.rendering != RENDERING_NONE) {
        draw_destroy(drawer);
    }
    game_destroy(&game);

    return r;
}
