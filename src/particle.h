#ifndef __particle_h
#define __particle_h

#include "vector.h"

struct particle {
    struct vector pos;
    /* intermediate position - before collision detection */
    struct vector inter_pos;
    struct vector heading;
    /*
     * Particle health: value between 0 and 1. Zero means dead, 1 means totally
     * healthy.
     */
    double health;
};

struct particle_action {
    enum {
        /* no action */
        NONE = 0,
        /* fire the gun towards current heading */
        FIRE,
        /* move towards current heading */
        MOVE,
        /* change heading */
        TURN,
        NUM_PARTICLE_ACTIONS
    } action;
    /*
     * Meaning depends on action:
     *  - FIRE: after action is done, represents the last position of bullet
     *          before hitting something
     *  - MOVE: none
     *  - TURN: the new heading
     */
    struct vector v;
};

#endif //__particle_h
