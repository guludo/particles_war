#include <ann.h>
#include <genetic_algorithm.h>
#include <errno.h>
#include <math.h>
#include <string.h>

#include "ann_team_controller.h"
#include "random.h"

static ann_activation_function_t activation_functions[] = {
    &ann_logistic_activation,
    &ann_logistic_activation,
    &ann_logistic_activation
};

struct annctl_data {
    struct annctl_params params;
    ann_multilayer_perc_t *anns;
    double *particles_score;
    struct ann_mlp_weights_info ann_weights_info;
    /* genetic algorithm for training */
    struct genetic_algorithm ga;
    int input_length;
    struct particle_action *last_actions;
    int has_trained;
};

void annctl_init_params(struct annctl_params *params)
{
    params->input_mask = ANNCTL_INPUT_DEFAULT;
    params->particle_map_range = ANNCTL_MAP_RANGE;
    params->num_sensed_particles = ANNCTL_NUM_SENSED_PARTICLES;
}

static inline int map_input_length(struct annctl_params *params)
{
    int side = 2 * params->particle_map_range + 1;
    return side * side; /* number of cells the particle can see*/
}
static inline int sensed_particles_input_length(struct annctl_params *params)
{
    /* x,y from pos; x,y from heading; health */
    int one_particle_state_len = 5;
    return params->num_sensed_particles * one_particle_state_len;
}
static inline int game_params_input_length()
{
    return 3; /* current three values in struct game_params */
}
static inline int last_action_input_length()
{
    return 3; /* one for action and two for vector v */
}

int annctl_input_length(struct annctl_params *params)
{
    int len = 0;
    unsigned long long m = params->input_mask;

    if (m & ANNCTL_INPUT_MAP)
        len += map_input_length(params);

    if (m & ANNCTL_INPUT_SENSED_PARTICLES)
        len += sensed_particles_input_length(params);

    if (m & ANNCTL_INPUT_GAME_PARAMS)
        len += game_params_input_length();

    if (m & ANNCTL_INPUT_LAST_ACTION)
        len += last_action_input_length();

    return len;
}

static void prepare_map_input(struct team_controller *c,
                              struct particle *p,
                              double *input)
{
    struct map *m = &c->game->map;
    struct annctl_data *d = c->data;
    struct annctl_params *params = &d->params;
    int y, x;
    int l = params->particle_map_range * 2 + 1;
    int y0 = p->pos.y - params->particle_map_range;
    int x0 = p->pos.x - params->particle_map_range;
    for (y = y0; y - y0 < l; y++) {
        for (x = x0; x - x0 < l; x++) {
            double input_value = 0;
            /* consider invalid locations as blocks */
            if (0 <= x && x < m->x_len && 0 <= y && y < m->y_len) {
                enum map_cell c = map_cell(m, x, y);
                switch (c) {
                case EMPTY:
                    input_value = 0;
                    break;
                case BLOCK:
                    input_value = 1;
                    break;
                }
            }

            *input = input_value;
            input++;
        }
    }
}

static void particle_input(struct team_controller *c,
                           struct particle *p,
                           double *input)
{
    input[0] = (double)p->pos.x / c->game->map.x_len;
    input[1] = (double)p->pos.y / c->game->map.y_len;
    input[2] = p->heading.x / ANNCTL_INPUT_HEADING_DIVIDER;
    input[3] = p->heading.y / ANNCTL_INPUT_HEADING_DIVIDER;
    input[4] = p->health;
}

/*
 * get the closest living particles
 */
static void prepare_sensed_particles_input(struct team_controller *c,
                                           struct particle *p,
                                           double *input)
{
    struct annctl_data *d = c->data;
    struct annctl_params *params = &d->params;
    int max_len = params->num_sensed_particles;
    struct {
        int distance;
        struct particle *p;
    } list[max_len];
    int len = 0;
    int i;

    /* first wee need to get list of closest particles */
    game_each_particle(c->game, q)
        /* loop until we fill the number of particles */
        if (len == max_len) break;
        int distance;
        if (q == p || q->health <= 0)
            continue;
        distance = abs(q->pos.x - p->pos.x) + abs(q->pos.y - q->pos.x);
        /* insert/sort items */
        for (i = 0; i < len; i++) {
            int j;
            if (distance < list[i].distance) {
                /* shift items */
                for (j = len - 1; j >= i; j--) {
                    /* if window is full, discard last particle */
                    if (j == max_len -1)
                        continue;
                    list[j + 1] = list[j];
                }
                break;
            }
        }
        list[i].p = q;
        list[i].distance = distance;
        len++;
    game_each_particle_end

    /* now we fill the input */
    for (i = 0; i < max_len; i++) {
        /* Use of i % len: reuse data in case we didn't find all necessary
         * particles */
        particle_input(c, list[i % len].p, input);
        input += 5;
    }
}

static void prepare_game_params_input(struct team_controller *c,
                                      struct game_params *params,
                                      double *input)
{
    input[0] = (double)params->bullet_range / (c->game->map.x_len + c->game->map.y_len);
    input[1] = params->bullet_damage;
    input[2] = params->max_turns / ANNCTL_INPUT_MAX_TURNS_DIVIDER;
}

static void prepare_last_action_input(struct team_controller *c,
                                      struct particle *p,
                                      double *input)
{
    struct annctl_data *d = c->data;
    struct particle_action *a = d->last_actions + (p - c->team->particles);
    input[0] = (double)a->action / NUM_PARTICLE_ACTIONS;
    if (a->action == TURN) {
        input[1] = a->v.x / ANNCTL_INPUT_HEADING_DIVIDER;
        input[2] = a->v.y / ANNCTL_INPUT_HEADING_DIVIDER;
    } else {
        input[1] = input[2] = 0;
    }
}

static void prepare_input(struct team_controller *c,
                          struct particle *p,
                          double *input)
{
    struct annctl_data *d = c->data;
    struct annctl_params *params = &d->params;
    unsigned long long m = params->input_mask;
    double *input_head = input;

    if (m & ANNCTL_INPUT_MAP) {
        prepare_map_input(c, p, input_head);
        input_head += map_input_length(params);
    }

    if (m & ANNCTL_INPUT_SENSED_PARTICLES) {
        prepare_sensed_particles_input(c, p, input_head);
        input_head += sensed_particles_input_length(params);
    }

    if (m & ANNCTL_INPUT_GAME_PARAMS) {
        prepare_game_params_input(c, &c->game->params, input_head);
        input_head += game_params_input_length();
    }

    if (m & ANNCTL_INPUT_LAST_ACTION) {
        prepare_last_action_input(c, p, input_head);
    }
}

static void particle_action(struct team_controller *c,
                            struct particle *p,
                            struct particle_action *action)
{
    struct annctl_data *d = c->data;
    int max_size = d->input_length > ANNCTL_INTER_OUTPUT1_LEN ?
            d->input_length : ANNCTL_INTER_OUTPUT1_LEN;
    if (max_size < ANNCTL_INTER_OUTPUT2_LEN)
        max_size = ANNCTL_INTER_OUTPUT2_LEN;

    double input[max_size], output[max_size];

    prepare_input(c, p, input);

    /* execute the neural network */
    ann_mlp(d->anns + (p - c->team->particles), input, output);

    /* first element: the action */
    action->action = floor(output[0] * NUM_PARTICLE_ACTIONS);
    if (action->action == NUM_PARTICLE_ACTIONS)
        action->action = NUM_PARTICLE_ACTIONS - 1;

    /* if turn, we need to get the new heading */
    if (action->action == TURN) {
        action->v.x = floor(-1 + output[1] * 3);
        action->v.y = floor(-1 + output[2] * 3);
    }
}

static void acc_particles_score(struct team_controller *c)
{
    struct annctl_data *d = c->data;
    double *scores = d->particles_score;
    struct game_event *events = c->game->turn_events.events;
    int num_events = c->game->turn_events.num_events;
    int i;

    for (i = 0; i < num_events; i++) {
        struct game_event *e = events + i;
        int origin_offset = e->origin_particle - c->team->particles;
        int origin_is_friend =
                0 <= origin_offset && origin_offset < c->team->num_particles;
        int target_offset = e->particle - c->team->particles;
        int target_is_friend =
                0 <= target_offset && target_offset < c->team->num_particles;

        switch (e->type) {
        case EVENT_PARTICLE_SHOT:
            if (origin_is_friend) {
                if (target_is_friend)
                    scores[origin_offset] -= 1.0;
                else
                    scores[origin_offset] += 1.0;
            } else {
                if (target_is_friend)
                    scores[target_offset] -= 1.0;
            }
            break;
        case EVENT_PARTICLE_KILLED:
            if (origin_is_friend) {
                if (target_is_friend)
                    scores[origin_offset] -= 1.0;
                else
                    scores[origin_offset] += 1.0;
            } else {
                if (target_is_friend)
                    scores[target_offset] -= 1.0;
            }
            break;
        }
    }
}

static void actions(struct team_controller *c,
                     struct particle_action *actions)
{
    struct annctl_data *d = c->data;
    int i;
    for (i = 0; i < c->team->num_particles; i++) {
        particle_action(c, c->team->particles + i, actions + i);
    }

    /* accumulate score */
    acc_particles_score(c);

    memcpy(d->last_actions,
           actions,
           c->team->num_particles * sizeof(struct particle_action));
}

static void write_training_weights(struct annctl_data *d)
{
    int i;
    for (i = 0; i < d->ga.population_size; i++) {
        ann_mlp_weights_write(d->anns + i, d->ga.population[i].solution);
    }
}

static void match_started(struct team_controller *c)
{
    struct annctl_data *d = c->data;
    if (c->training) {
        memset(d->particles_score, 0, sizeof(double) * c->team->num_particles);
    } else if (d->has_trained) {
        d->has_trained = 0;
    }
}

static void match_finished(struct team_controller *c)
{
    if (c->training) {
        struct annctl_data *d = c->data;
        ga_iterate(&d->ga);
        write_training_weights(d);
        d->has_trained = 1;
    }
}

static void ga_init_solution(struct genetic_algorithm *ga, void *solution)
{
    double *weights = solution;
    struct team_controller *c = ga->user_data;
    struct annctl_data *d = c->data;
    struct ann_mlp_weights_info *info = &d->ann_weights_info;
    size_t i;
    for (i = 0; i < info->length; i++) {
        weights[i] = -1 + 2 * randomd();
    }
}

static double ga_evaluate(struct genetic_algorithm * ga, void * solution)
{
    int i = (int)ga_solution_index(ga, solution);
    struct team_controller *c = ga->user_data;
    struct annctl_data *d = c->data;
    return d->particles_score[i];
}

static void ga_mutate(struct genetic_algorithm * ga,
                      ga_solution_data_t * solution_data)
{
    double *weights = solution_data->solution;
    struct team_controller *c = ga->user_data;
    struct annctl_data *d = c->data;
    struct ann_mlp_weights_info *info = &d->ann_weights_info;
    size_t i;
    for (i = 0; i < info->length; i++)
        if (randomd() < .2)
            weights[i] = -1 + 2 * randomd();
}

void ga_crossover(struct genetic_algorithm * ga,
                  ga_solution_data_t * parent_a_data,
                  ga_solution_data_t * parent_b_data,
                  ga_solution_data_t * child_a_data,
                  ga_solution_data_t * child_b_data)
{
    struct team_controller *c = ga->user_data;
    struct annctl_data *d = c->data;
    struct ann_mlp_weights_info *info = &d->ann_weights_info;
    size_t i;
    double *parent_a = parent_a_data->solution,
           *parent_b = parent_b_data->solution,
           *child_a = child_a_data->solution,
           *child_b = child_b_data->solution;

    for (i = 0; i < info->length; i++) {
        if (ga_random_double(ga) < .5) {
            child_a[i] = parent_a[i];
            child_b[i] = parent_b[i];
        } else {
            child_a[i] = parent_b[i];
            child_b[i] = parent_a[i];
        }
    }
}

static void ga_each_generation(struct genetic_algorithm *ga)
{
    unsigned int i;
    double sum = 0;
    for (i = 0; i < ga->population_size; i++) {
        sum += ga->population[i].fitness;
    }

    fprintf(stdout, "ann_team_controller: avg_fitness=%lf\n",
            sum / ga->population_size);
    fprintf(stdout, "ann_team_controller: best_fitness=%lf\n",
            ga->best_solution_data.fitness);
}

int annctl(struct team_controller *c, struct annctl_params *params)
{
    int r = 0;
    int e = 0;
    int i;
    /*
     * Three final outputs:
     *  - The action
     *  - x value for action vector v
     *  - y value for action vector v
     */
    int output_lengths[] = {
        ANNCTL_INTER_OUTPUT1_LEN,
        ANNCTL_INTER_OUTPUT2_LEN,
        3
    };
    struct annctl_data *d = malloc(sizeof(*d));

    c->name = "ann";

    if (d == NULL)
        return -1;
    c->data = d;

    d->params = *params;
    d->input_length = annctl_input_length(params);
    d->has_trained = 0;

    d->last_actions =
            malloc(c->team->num_particles * sizeof(struct particle_action));
    if (d->last_actions == NULL)
        goto annctl_error_cleanup;
    for (i = 0; i < c->team->num_particles; i++)
        d->last_actions[i].action = NONE;

    d->anns = malloc(sizeof(*(d->anns)) * c->team->num_particles);
    if (d->anns == NULL)
        goto annctl_error_cleanup;
    for (i = 0; i < c->team->num_particles; i++) {
        r = ann_mlp_init(d->anns + i, d->input_length, output_lengths, 3);
        if (r < 0) {
            int j = 0;
            int e = errno;
            for (j = 0; j < i; j++)
                ann_mlp_destroy(d->anns + j);
            errno = e;
            goto annctl_error_cleanup;
        }
        ann_mlp_activation_functions(d->anns + i, activation_functions);
    }

    r = ann_mlp_weights_info(d->anns, &d->ann_weights_info);
    if (r < 0)
        goto annctl_error_cleanup;

    d->particles_score = malloc(sizeof(double) * c->team->num_particles);
    if (d->particles_score == NULL)
        goto annctl_error_cleanup;

    c->actions = &actions;
    c->match_started = &match_started;
    c->match_finished = &match_finished;

    /* init training GA */
    r = ga_init(&d->ga,
                sizeof(double) * d->ann_weights_info.length,
                c->team->num_particles);
    if (r < 0)
        goto annctl_error_cleanup;
    d->ga.user_data = c;
    d->ga.init_solution = &ga_init_solution;
    d->ga.mutate = &ga_mutate;
    d->ga.crossover = &ga_crossover;
    d->ga.evaluate = &ga_evaluate;
    d->ga.on_each_generation = &ga_each_generation;

    ga_reset(&d->ga);
    write_training_weights(d);

    return 0;

annctl_error_cleanup:
    e = errno;
    if (c->data != NULL) {
        if (d->anns != NULL) {
            ann_mlp_weights_info_destroy(&d->ann_weights_info);
            free(d->anns);
            d->anns = NULL;
        }
        if (d->particles_score != NULL) {
            free(d->particles_score);
            d->particles_score = NULL;
        }
        free(c->data);
        c->data = NULL;
    }
    errno = e;
    return -1;
}

int annctl_destroy(struct team_controller *c)
{
    struct annctl_data *d = c->data;
    int i;
    ann_mlp_weights_info_destroy(&d->ann_weights_info);
    for (i = 0; i < c->team->num_particles; i++) {
        ann_mlp_destroy(d->anns + i);
    }
    free(d->anns);
    free(d->last_actions);
    free(c->data);
    c->data = NULL;
    return 0;
}
