#ifndef __ann_team_controller_h
#define __ann_team_controller_h

#include "game.h"

/* Input: the cell value for the position on the map */
#define ANNCTL_INPUT_MAP 0x1
/* Input: all state fields of sensed particles  */
#define ANNCTL_INPUT_SENSED_PARTICLES 0x2
/* Input: all game params */
#define ANNCTL_INPUT_GAME_PARAMS 0x3
/* Input: particles last action value and also the value of member v */
#define ANNCTL_INPUT_LAST_ACTION 0x3
#define ANNCTL_INPUT_ALL (~0u)

#define ANNCTL_INPUT_DEFAULT (ANNCTL_INPUT_ALL & ~ANNCTL_INPUT_LAST_ACTION)

#define ANNCTL_MAP_RANGE 2
#define ANNCTL_NUM_SENSED_PARTICLES 4

/* Must be at least 6 */
#define ANNCTL_INTER_OUTPUT1_LEN 24
#define ANNCTL_INTER_OUTPUT2_LEN 12

#define ANNCTL_INPUT_MAX_TURNS_DIVIDER 10000.0
#define ANNCTL_INPUT_HEADING_DIVIDER 3.0

struct annctl_params {
    unsigned long input_mask;
    int particle_map_range;
    int num_sensed_particles;
};

void annctl_init_params(struct annctl_params *params);
int annctl(struct team_controller *controller, struct annctl_params *params);
int annctl_destroy(struct team_controller *controller);

#endif //__ann_team_controller_h
