#include <math.h>

#include "game.h"
#include "greedy_team_controller.h"
#include "random.h"

static void actions(struct team_controller *c,
                     struct particle_action *actions)
{
    int i;
    for (i = 0; i < c->team->num_particles; i++) {
        struct particle *p = c->team->particles + i;
        struct particle *q = shot_hits_particle(c->game, p, NULL);
        int q_offset = q - c->opponent->particles;

        /* if enemy locked */
        if (q != NULL &&
            0 <= q_offset && q_offset < c->opponent->num_particles) {
            actions[i].action = FIRE;
            continue;
        }

        actions[i].action = randomd() * NUM_PARTICLE_ACTIONS;

        if (actions[i].action == FIRE) actions[i].action = MOVE;

        if (actions[i].action == TURN) {
            /* set heading pointing towards the enemy */
            int j;
            int base = (int)round(randomd() * c->opponent->num_particles);
            for (j = 0; j < c->opponent->num_particles; j++) {
                int dx, dy;
                q = c->opponent->particles +
                        (base + j) % c->opponent->num_particles;
                if (q->health <= 0)
                    continue;

                dx = q->pos.x - p->pos.x;
                if (dx == 0)
                    actions[i].v.x = 0;
                else if (dx > 0)
                    actions[i].v.x = 1;
                else
                    actions[i].v.x = -1;

                dy = q->pos.y - p->pos.y;
                if (dy == 0)
                    actions[i].v.y = 0;
                else if (dy > 0)
                    actions[i].v.y = 1;
                else
                    actions[i].v.y = -1;

                break;
            }

            if (j == c->opponent->num_particles)
                actions[i].action = NONE;
        }
    }
}

void greedy_team_controller(struct team_controller *c)
{
    c->name = "greedy";
    c->actions = &actions;
}
