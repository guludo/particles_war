#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "game.h"
#include "random.h"
#include "random_team_controller.h"

/*
 * Number max of events for a game:
 *
 *  - Each particle can only shoot once a turn, so max number of
 *  EVENT_PARTICLE_SHOT events is the total number of particles.
 *
 *  - Number max of kills (EVENT_PARTICLE_KILLED) in a turn is the total number
 *  of particles.
 */
#define GAME_MAX_TURN_EVENTS(game) \
    (2 * (game->team_a.num_particles + game->team_b.num_particles))

int game_init(struct game *game, int num_particles_a, int num_particles_b)
{
    game->team_a.particles = calloc(num_particles_a + num_particles_b,
                                    sizeof(struct particle));
    if (game->team_a.particles == NULL) {
        errno = ENOMEM;
        return -1;
    }
    game->team_b.particles = game->team_a.particles + num_particles_a;

    game->team_a.num_particles = num_particles_a;
    game->team_b.num_particles = num_particles_b;

    game->team_a_controller.game = game;
    game->team_a_controller.team = &game->team_a;
    game->team_a_controller.opponent = &game->team_b;
    game->team_a_controller.training = 0;
    game->team_a_controller.match_started = NULL;
    game->team_a_controller.match_finished = NULL;
    random_team_controller(&game->team_a_controller);

    game->team_b_controller.game = game;
    game->team_b_controller.team = &game->team_b;
    game->team_b_controller.opponent = &game->team_a;
    game->team_b_controller.training = 0;
    game->team_b_controller.match_started = NULL;
    game->team_b_controller.match_finished = NULL;
    random_team_controller(&game->team_b_controller);

    game->params.bullet_range = GAME_PARAM_BULLET_RANGE;
    game->params.bullet_damage = GAME_PARAM_BULLET_DAMAGE;
    game->params.max_turns = GAME_PARAM_MAX_TURNS;

    game->callbacks.start = NULL;
    game->callbacks.turn = NULL;
    game->callbacks.finish = NULL;

    errno = 0;
    return 0;
}

static int sort_particles_positions(struct game *game)
{
    struct map *map = &game->map;
    int busy_map[map->x_len * map->y_len];
    int num_free_cells = 0;
    int i, x, y;
    int total_particles =
            game->team_a.num_particles + game->team_b.num_particles;
    /* space on x-axis to spread particles of team a and b */
    int x_space_a = 0, x_space_b = 0;
    int free_cells_a = 0, free_cells_b = 0;
    int required_free_cells_a =
            game->team_a.num_particles * GAME_MAP_PARTICLE_ROOM,
        required_free_cells_b =
            game->team_b.num_particles * GAME_MAP_PARTICLE_ROOM;

    for (x = 0; x < map->x_len / 2; x++) {
        for (y = 0; y < map->y_len; y++) {
            int index_a = x * map->y_len + y;
            int index_b = (map->x_len - x - 1) * map->y_len + y;

            if (game->map.cells[index_a] == EMPTY) {
                num_free_cells++;
                if (free_cells_a < required_free_cells_a) {
                    free_cells_a++;
                    if (free_cells_a == required_free_cells_a) {
                        x_space_a = x + 1;
                    }
                }
                busy_map[index_a] = 0;
            } else {
                busy_map[index_a] = 1;
            }

            if (game->map.cells[index_b] == EMPTY) {
                num_free_cells++;
                if (free_cells_b < required_free_cells_b) {
                    free_cells_b++;
                    if (free_cells_b == required_free_cells_b) {
                        x_space_b = x + 1;
                    }
                }
                busy_map[index_b] = 0;
            } else {
                busy_map[index_b] = 1;
            }
        }
    }

    if (map->x_len % 2 != 0) { /* if x dimension is odd sized */
        x = map->x_len / 2 + 1;
        for (y = 0; y < map->y_len; y++) {
            int index = x * map->y_len + y;

            if (game->map.cells[index] == EMPTY) {
                num_free_cells++;

                if (free_cells_a < required_free_cells_a) {
                    free_cells_a++;
                    if (free_cells_a == required_free_cells_a) {
                        x_space_a = x + 1;
                    }
                }
                if (free_cells_b < required_free_cells_b) {
                    free_cells_b++;
                    if (free_cells_b == required_free_cells_b) {
                        x_space_b = x + 1;
                    }
                }

                busy_map[index] = 0;
            } else {
                busy_map[index] = 1;
            }
        }
    }

    /* leave room of 3 cells for each particle */
    if (GAME_MAP_PARTICLE_ROOM * total_particles > num_free_cells) {
        errno = EINVAL;
        fprintf(stderr, "game: map to small for %d particles\n",
                total_particles);
        return -1;
    }

    for (i = 0; i < game->team_a.num_particles; i++) {
        struct particle *p = game->team_a.particles + i;
        x = randomd() * x_space_a;
        y = randomd() * map->y_len;
        while (busy_map[x * map->y_len + y]) {
            x = randomd() * x_space_a;
            y = randomd() * map->y_len;
        }
        p->pos.x = x;
        p->pos.y = y;
        p->heading.x = 1;
        p->heading.y = 0;
        p->inter_pos = p->pos;
        busy_map[x * map->y_len + y] = 1;
    }
    for (i = 0; i < game->team_b.num_particles; i++) {
        struct particle *p = game->team_b.particles + i;
        x = randomd() * x_space_b;
        y = randomd() * map->y_len;
        while (busy_map[x * map->y_len + y]) {
            x = randomd() * x_space_b;
            y = randomd() * map->y_len;
        }
        p->pos.x = map->x_len -x -1;
        p->pos.y = y;
        p->heading.x = -1;
        p->heading.y = 0;
        p->inter_pos = p->pos;
        busy_map[x * map->y_len + y] = 1;
    }

    errno = 0;
    return 0;
}

struct particle *shot_hits_particle(struct game *g, struct particle *shooter,
                                    struct vector *bullet_last_position)
{
    struct vector b = shooter->pos; /* the bullet */
    struct particle *q = NULL;
    int i;
    /* find first hit */
    for (i = 0; i < g->params.bullet_range; i++) {
        enum map_cell c;
        int index;

        b.x += shooter->heading.x;
        b.y += shooter->heading.y;

        if (0 > b.x || b.x >= g->map.x_len)
            break;
        if (0 > b.y || b.y >= g->map.y_len)
            break;

        index = b.x * g->map.y_len + b.y;

        c = g->map.cells[index];
        if (c != EMPTY) /* bullet hits something in map */
            break;

        q = g->particle_map[index];
        if (q != NULL) /* bullet hits a particle */
            break;
    }

    /* "last" position of bullet */
    if (bullet_last_position != NULL) {
        bullet_last_position->x = b.x - shooter->heading.x;
        bullet_last_position->y = b.y - shooter->heading.y;
    }

    return q;
}

static void particle_do_action(struct game *g, struct particle *p,
                               struct particle_action *action)
{
    if (p->health == 0) {
        action->action = NONE;
        return;
    }

    switch (action->action) {
        case MOVE:
            p->inter_pos.x = p->pos.x + p->heading.x;
            p->inter_pos.y = p->pos.y + p->heading.y;

            if (p->inter_pos.x < 0)
                p->inter_pos.x = 0;
            else if (p->inter_pos.x >= g->map.x_len)
                p->inter_pos.x = g->map.x_len - 1;

            if (p->inter_pos.y < 0)
                p->inter_pos.y = 0;
            else if (p->inter_pos.y >= g->map.y_len)
                p->inter_pos.y = g->map.y_len - 1;

            break;
        case TURN:
            p->heading = action->v;
            break;
        case FIRE:
        {
            struct particle *q = shot_hits_particle(g, p, &action->v);
            if (q != NULL) { /* bullet hit a particle */
                struct game_event *e =
                        g->turn_events.events + g->turn_events.num_events++;
                q->health -= g->params.bullet_damage;

                e->type = EVENT_PARTICLE_SHOT;
                e->particle = q;
                e->origin_particle = p;

                if (q->health <= 0) { /* if particle was killed */
                    int index = q->pos.x * g->map.y_len + q->pos.y;
                    q->health = 0;
                    g->particle_map[index] = NULL;

                    e++;
                    g->turn_events.num_events++;
                    e->type = EVENT_PARTICLE_KILLED;
                    e->particle = q;
                    e->origin_particle = p;
                }
            }
            break;
        }
        default:
            break;
    }
}

static int particle_has_collision(struct game *g, struct particle *p)
{
    struct vector v = p->inter_pos;
    if (g->map.cells[v.x * g->map.y_len + v.y] != EMPTY)
        return 1;

    game_each_particle(g, q)
        if (q == p)
            continue;
        if (p->inter_pos.x == q->inter_pos.x &&
            p->inter_pos.y == q->inter_pos.y)
            return 1;
    game_each_particle_end;
    return 0;
}

static void particles_commit_positions(struct game *g)
{
    game_each_particle(g, p)
        if (p->health == 0)
            continue;
        if (!particle_has_collision(g, p)) {
            g->particle_map[p->pos.x * g->map.y_len + p->pos.y] = NULL;
            p->pos = p->inter_pos;
            g->particle_map[p->pos.x * g->map.y_len + p->pos.y] = p;
        }
    game_each_particle_end;
}

int game_run(struct game *game)
{
    int r, i, l;
    struct particle_action actions_a[game->team_a.num_particles];
    struct particle_action actions_b[game->team_b.num_particles];
    struct particle *particle_map[game->map.x_len * game->map.y_len];
    struct game_event turn_events[GAME_MAX_TURN_EVENTS(game)];

    game->actions_a = actions_a;
    game->actions_b = actions_b;
    game->turn_events.events = turn_events;
    game->turn_events.num_events = 0;

    game->particle_map = particle_map;

    l = game->map.x_len * game->map.y_len;
    for (i = 0; i < l; i++)
        game->particle_map[i] = NULL;

    r = sort_particles_positions(game);
    if (r < 0)
        return r;

    game_each_particle(game, p)
        p->health = 1;
        game->particle_map[p->pos.x * game->map.y_len + p->pos.y] = p;
    game_each_particle_end;


    game->turn = 0;

    if (game->callbacks.start != NULL) {
        r = game->callbacks.start(game);
        if (r != 0) {
            int e = errno;
            fprintf(stderr, "game: start callback returned error status: %m\n");
            errno = e;
            return r;
        }
    }

    if (game->team_a_controller.match_started != NULL)
        game->team_a_controller.match_started(&game->team_a_controller);
    if (game->team_b_controller.match_started != NULL)
        game->team_b_controller.match_started(&game->team_b_controller);

    while (game->turn < game->params.max_turns) {
        int dead_count_a = 0, dead_count_b = 0;
        game->team_a_controller.actions(&game->team_a_controller, actions_a);
        game->team_b_controller.actions(&game->team_b_controller, actions_b);

        game->turn_events.num_events = 0;

        for (i = 0; i < game->team_a.num_particles; i++) {
            struct particle *p = game->team_a.particles + i;
            struct particle_action *action = actions_a + i;
            particle_do_action(game, p, action);
        }
        for (i = 0; i < game->team_b.num_particles; i++) {
            struct particle *p = game->team_b.particles + i;
            struct particle_action *action = actions_b + i;
            particle_do_action(game, p, action);
        }
        particles_commit_positions(game);

        if (game->callbacks.turn != NULL)
            game->callbacks.turn(game);

        game->turn++;

        for (i = 0; i < game->team_a.num_particles; i++) {
            struct particle *p = game->team_a.particles + i;
            if (p->health <= 0)
                dead_count_a++;
        }
        for (i = 0; i < game->team_b.num_particles; i++) {
            struct particle *p = game->team_b.particles + i;
            if (p->health <= 0)
                dead_count_b++;
        }
        if (dead_count_a == game->team_a.num_particles ||
            dead_count_b == game->team_b.num_particles)
            break;
    }

    if (game->team_a_controller.match_finished != NULL)
        game->team_a_controller.match_finished(&game->team_a_controller);
    if (game->team_b_controller.match_finished != NULL)
        game->team_b_controller.match_finished(&game->team_b_controller);

    if (game->callbacks.finish != NULL)
        game->callbacks.finish(game);

    return 0;
}

int game_destroy(struct game *game)
{
    free(game->team_a.particles);
    game->team_a.particles = NULL;
    game->team_b.particles = NULL;
    game->team_a.num_particles = 0;
    game->team_b.num_particles = 0;

    errno = 0;
    return 0;
}
