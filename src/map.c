#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "map.h"

static int dimension_from_stream(FILE *stream, int *x, int *y)
{
    int lines, columns = 0;
    int c, line_columns = 0;
    int head = ftell(stream);

    errno = 0;

    do {
        c = fgetc(stream);
        switch (c) {
        case '\n':
            lines++;
            if (line_columns > columns)
                columns = line_columns;
            line_columns = 0;
            break;
        case '#':
        case ' ':
            line_columns++;
            break;
        default:
            errno = EINVAL;
            return -1;
        }
    } while (c != EOF);

    if (lines == 0 && columns == 0) {
        errno = EINVAL;
        return -1;
    }

    if (columns == 0 && lines == 0) {
        errno = EINVAL;
        return -1;
    }

    if (columns > 0)
        lines++;

    *x = columns;
    *y = lines;

    fseek(stream, head, SEEK_SET);

    return 0;
}

int map_from_stream(struct map *map, FILE *stream)
{
    int c, i = 0, j = 0;

    if (dimension_from_stream(stream, &map->x_len, &map->y_len) < 0)
        return -1;

    map->cells = calloc(map->x_len * map->y_len, sizeof(enum map_cell));
    if (map->cells == NULL)
        return -1;

    while ((c = fgetc(stream)) != EOF) {
        switch (c) {
        case '\n':
            i++;
            j = 0;
            break;
        case '#':
            map->cells[i * map->x_len + j] = BLOCK;
            j++;
            break;
        default:
            errno = EINVAL;
            free(map->cells);
            return -1;
        }
    }
    return 0;
}

int map_init_empty(struct map *map, int x_len, int y_len)
{
    int i;
    int len = x_len * y_len;
    map->x_len = x_len;
    map->y_len = y_len;
    map->cells = malloc(len * sizeof(enum map_cell));
    if (map->cells == NULL) {
        errno = ENOMEM;
        return -1;
    }

    for (i = 0; i < len; i++)
        map->cells[i] = EMPTY;

    errno = 0;
    return 0;
}
