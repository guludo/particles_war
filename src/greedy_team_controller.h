#ifndef __greedy_team_controller_h
#define __greedy_team_controller_h

#include "game.h"
#include "particle.h"

void greedy_team_controller(struct team_controller *c);

#endif //__greedy_team_controller_h
