#include <stdlib.h>

#include "random.h"

double randomd()
{
    return (double)random() / RAND_MAX;
}
