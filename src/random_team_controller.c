#include "random.h"
#include "random_team_controller.h"

static struct vector headings[8] = {
    { 1,  0},
    { 1,  1},
    { 0,  1},
    {-1,  1},
    {-1,  0},
    {-1, -1},
    { 0, -1},
    { 1, -1}
};

static void actions(struct team_controller *c,
                     struct particle_action *actions)
{
    int i;
    for (i = 0; i < c->team->num_particles; i++) {
        actions[i].action = randomd() * NUM_PARTICLE_ACTIONS;

        if (actions[i].action == TURN) {
            actions[i].v = headings[(int)(randomd() * 8)];
        }
    }
}

void random_team_controller(struct team_controller *c)
{
    c->name = "random";
    c->actions = &actions;
}
