#ifndef __random_team_controller_h
#define __random_team_controller_h

#include "game.h"
#include "particle.h"

void random_team_controller(struct team_controller *c);

#endif //__random_team_controller_h
