#ifndef __map_h
#define __map_h

#include <stdio.h>

enum map_cell {
    EMPTY = ' ',
    BLOCK = '#'
};

struct map {
    int x_len;
    int y_len;
    /* x major order, i.e., use (x * y_len + y) for index at [x,y] */
    enum map_cell *cells;
};

#define map_cell(map, x, y) ((map)->cells[(x) * (map)->y_len + (y)])

int map_from_stream(struct map *map, FILE *stream);
int map_init_empty(struct map *map, int x_len, int y_len);

#endif //__map_h
