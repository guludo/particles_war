#!/bin/python

def options(opt):
    opt.load('compiler_c')
    opt.recurse(
        mandatory=False,
        dirs='src',
    )


def configure(cfg):
    cfg.load('compiler_c')
    cfg.recurse(
        mandatory=False,
        dirs='src',
    )


def build(bld):
    bld.recurse(
        mandatory=False,
        dirs='src',
    )
